export const DIRECTIONS = {
  UP: 'up',
  DOWN: 'down',
  LEFT: 'left',
  RIGHT: 'right',
}

export const PLACEMENTS = {
  BEFORE: 'before',
  AFTER: 'after',
}
